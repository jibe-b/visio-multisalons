import React from "react"

export default props => {

    return (
        <div style={{
            position: "absolute",
            top: "25%",
            right: "4%",
            width:"20%"
        }}>
            <p>
                Bienvenue ! Dans le canal accueil, vous serez accueillie, ça va être bref, c'est juste pour vous donner les infos !
            </p>
            <p>
                Pour quitter un salon et passer à un autre, il vous suffit de cliquer sur le nom du salon.
            </p>
            <p>
                Le canal plénière est celui où chacun·e est invité·e à discuter tou·te·s ensemble. Veillez bien à y demander la parole et à attendre qu'on vous la donne !
            </p>
            <p>
                La parole sera plus libre dans les canaux par région et par atelier !
            </p>
        </div>
    )
}